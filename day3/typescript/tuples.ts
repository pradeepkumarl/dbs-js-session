let users: string[];

users = ['naveen', 'praveen', 'usha'];

users.forEach(u => console.log(u));

const user: [number, string] = [23, "Pramod"];
user[0] = 3444;
user[1] = "Praveen";

const [id, username] = user;

console.log(`The user with id of ${id} having his name as ${username}`);
