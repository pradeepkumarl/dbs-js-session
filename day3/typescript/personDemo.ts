import Person, {NATIONALITY as N, validateUser as vUser} from './Person';

const naveen = new Person("Naveen", 22, 25000);
const ravitej = new Person("Ravitej");
const kiran = new Person("Kiran", undefined, 25000);

naveen.salary = 45000;
kiran.age = 45;
kiran.name = "KIRAN";


const printPersonDetails = function (person : Person) {
    console.log(person.name);
    console.log(`nationality is ${N}`)
    console.log("Calling the function " + vUser(person))
}
printPersonDetails(naveen);
