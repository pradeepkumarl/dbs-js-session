export default class Person {

    constructor(private _name : string, private _age? : number, private _salary? : number) {}

    get name(): string {
        return this._name;
    }

    set name(name : string) {
        console.log("setting the value of name : " + name);
        this._name = name;
    }

    get age(): number {
        return this._age;
    }

    set age(age : number) {
        this._age = age;
    }

    get salary(): number {
        return this._salary;
    }

    set salary(sal : number) {
        this._salary = sal;
    }
}


export const NATIONALITY = "INDIAN";

export const validateUser = function (user) {
    return user === null;
}
