import Person from "./Person";

export default interface CrudDAO < T > {

    save(t : T): void;
    listAll(): T[];
    findById(id : number): T;
    deleteById(id : number): void;
    update(id : number, t : T): T;
}

class ArrayBackedDAO<Person> implements CrudDAO < Person > {

    save(person : Person): void {}
    listAll(): []{
        return null;
    }
    findById(id : number): Person {
        return null;
    }
    deleteById(id : number): void {}
    update(id : number, t : Person): Person {
        return null
    };

}

class ItemRepo implements CrudDAO < Item > {
    save(item : Item): void {}
    listAll(): []{
        return null;
    }
    findById(id : number): Item {
        return null;
    }
    deleteById(id : number): void {}
    update(id : number, t : Item): Item {
        return null
    };
}

interface Item {
    id: number;
    name: string;
    price: number;
}

interface User {
    id: number;
    name: string
}

const iPhone: Item = {
    id: 34,
    name: 'Apple-Iphone',
    price: 45000
};

