const depts = ['HR', 'Finance', 'Payroll'];
const projectTeams = ['Medlife', 'Hitachi', 'TUI'];

const consolidatedTeams = [
    ... depts,
    ... projectTeams
]

console.log(consolidatedTeams)

const fruits = ['banana', 'grapes', 'mango', 'papaya'];
const veggies = ['tomato', 'brinjal', 'beans'];

const eateries = [
    ... fruits,
    'pakoda',
    'samosa',
    'idli',
    ... veggies
]

const karnataka = ['bangalore', 'mangalore', 'hubballi', 'belgaum'];
const cities = [... karnataka.slice(0, 2),...['Hyderabad', 'Vijaywada'],... karnataka.slice(3)];

console.log(cities);
