// two types of destrucuring. arrays and object

const team = ["Virat", "Rohit", "Raina", "Chahal"];

const captainStr = team[0];
const vCaptainStr = team[1];

const printDetails = function ([captain, vCapatin]) {
    console.log(`The captain is ${captain} and the vice captain is ${vCapatin}`);
}

// printDetails(team);

const data = '1111, Pradeep, bangalore, 36';
const [id, name, location, age] = data.split(',');

let batsman = 'Ganguly';
let runner = 'Sachin';

[runner, batsman] = [batsman, runner];

console.log(`After a single run, now the batsman is ${batsman} and runner is ${runner}`);
