function* generatorFn () {
    let index = 0
    while (index < 10) {
        yield Math.floor(Math.random() * 1000);
        index++;

    }
}

const gen = generatorFn();
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next().value);
console.log(gen.next());
