const players = ['Dhoni', 'Virat', 'Raina', 'Jadeja'];

const [captain, vCaptain, ...team] = players;

console.log(`The captain is ${captain} and the Vice Captain is ${vCaptain}`);
console.log(`The rest of the team members are ${team}`)


const printDetails = function ({
    name,
    age,
    ...args
}) {
    console.log(`Name: ${name}, Age: ${age}`);
    console.log(`The remaining attributes of the user is ${
        args.city
    }`)
    console.log(...arguments);
}

const user = {
    name: 'Pradeep',
    age: 44,
    city: 'Chennai'
}


printDetails(user);
