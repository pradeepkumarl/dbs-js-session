const outerFun = function (name, msg) {
    const outerFunVariable = "Outer Function variable";
    console.log("This is a outer functin ");
    console.log("Variables name and msg inside the outer function ", name, msg);


    const innerFun = () => {
        console.log("Inside the inner Function ");
        console.log("Variables name and msg inside the inner function ", name, msg);
        console.log("Inside the inner function, I can refer to the outer function variables ", outerFunVariable);
        console.log(this);
    }
     innerFun();
}


outerFun.call({
    key: 'ThisObject'
}, "Naveen", "Welcome");
