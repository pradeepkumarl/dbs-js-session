// order food
// order cool-drinks
// enjoy the party


const foodDelivery = new Promise((resolve, reject) => {
    setTimeout(() => reject("Food delivered !!"), 4000);
})

const beverages = new Promise((resolve, reject) => {
    setTimeout(() => resolve("Beverages delivered !!"), 4000);
})

const superPromise = Promise.all([foodDelivery, beverages]);

superPromise.then(([rs1, rs2]) => {
    console.log(rs1);
    console.log(rs2);
    console.log("Now enjoy the party !!");
}).catch(([error1, error2]) => {
    console.log("came inside the error block");
    console.log(error1);
})
