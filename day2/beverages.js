const cocaCola = new Promise((resolve, reject) => {
    setTimeout(() => resolve("Coke delivered !!"), 8000);
})

const pepsi = new Promise((resolve, reject) => {
    setTimeout(() => reject("There is some bad response !!"), 2000);
})


const superPromise = Promise.race([cocaCola, pepsi]);

superPromise.then(res => {
    console.log(res);
    console.log("Beverage is delivered ..")
}).catch(error => console.log(error));
