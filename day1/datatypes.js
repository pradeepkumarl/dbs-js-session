// single line comment
/*
  multi line comment
*/

// data types

var number = 56;
var char = "string ";
var und; // undefined
var explicitNull = null; // null
var flag = false;

var ages = [
    11,
    22,
    33,
    44,
    'tst'
];

ages[0];
ages.length;

number = "Kiran";
