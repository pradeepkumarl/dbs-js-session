/*
1. For the meal ordered there will be a total bill - Ex 400 
2. For the bill, calculate the service tax 
3. On top of service tax, you can optionally pay the tip  

Write a closure function, to apply tax and apply a tip only after which the 
transaction should be marked as completed.
*/
var totalBill = function (amount) {
    return function (taxInPcFunc) {
        return function (tipAmount) {
            return taxInPcFunc(amount) + tipAmount;
        }
    }
}

var taxPerctange18 = function (amount) {
    return(18 / 100 * amount) + amount;
}

var taxPerctange18 = genericTaxApply(18)(2000);
var taxPerctange20 = genericTaxApply(20)(2000);

var genericTaxApply = function (taxInPc) {
    return function (amount) {
        return taxInPc / 100 * amount + amount;

    }
}

var billAfterTax = totalBill(1000);
var billAfterTip = billAfterTax(taxPerctange18);

console.log("Total Bill " + billAfterTip(10));
