var funs = {
    getName: function () {
        return this.name;
    },

    setName: function (name) {
        this.name = name;
    }
}

var user = {
    name: "Pradeep"
};

var car = {
    name: "Baleno"
}

user.__proto__ = funs;
car.__proto__ = funs;

user.setName("Kishore");
console.log(user.getName());

console.log(car.getName());
