var users = [
    {
        name: 'Kiran',
        age: 34,
        location: 'Bangalore'
    },
    {
        name: 'Vinay',
        age: 24,
        location: 'Hyderabad'
    },


    {
        name: 'Usha',
        age: 44,
        location: 'Chennai'
    },
    {
        name: 'Vikram',
        age: 14,
        location: 'Mumbai'
    }, {
        name: 'Akash',
        age: 20,
        location: 'Bangalore'
    }
]

var sortUsersByAgeAsc = function (u1, u2) {
    return u1.age - u2.age;
}


var sortUsersByAgeDesc = function (u1, u2) {
    return u2.age - u1.age;
}

var sortUsersByNameAsc = function (u1, u2) {
    return u1.name.localeCompare(u2.name);
}

var sortUsersByNameDesc = function (u1, u2) {
    return -1 * (u1.name.localeCompare(u2.name));
}

var result = users.sort(sortUsersByAgeDesc)

result.forEach(function (val) {
    console.log(val);
});
