var ages = [
    34,
    5,
    6,
    38,
    76,
    2,
    30,
    22,
    24,13,14
];

var teenage = function (age) {
    return(age > 12 && age < 20);
}

var adult = function (age) {
    return age > 19;
}


var result = ages.filter(teenage);

var sortByAsc = ages.sort(function(a, b){
    return b - a;
})

console.log(sortByAsc);

