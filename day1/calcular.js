var sum = function (a, b) {
    return a + b;
}

var sub = function (a, b) {
    return a - b;
}

// function expression
var product = function (a, b) {
    return a * b;
}

var divide = function (a, b) {
    return a / b;
}

var modulo = function (a, b) {
    return a % b;
}

function apply(a, fun) {
    return fun(a);
}


var square = function (a) {
    return a * a;
}

var cube = function (a) {
    return a * a * a;
}
var result = apply(56, cube);

console.log('The result is ' + result);


// sub, product, divide, modulo
