var greet = function (msg) {
    var fun = function (name) {
        var salutationFun = function (salutation) {
            return salutation + " " + name + " " + msg + "!!";
        }
        return salutationFun;
    }
    return fun;
}
var result = greet("Welcome ");

var pradeepWelcome = result("Pradeep ");
var kiranWelcome = result("Kiran ");

console.log(pradeepWelcome("Mr "));
console.log(kiranWelcome(" Mr "));
