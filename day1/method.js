// Arithmetic operators
// relational operators
// ==, ===, != , !==

/*
  for type safety use typeOf operator
 */
function sum(a, b, c, d) {
    console.log('The sume is ' + (
        a + b
    ));
}

var sumFunc = function (a, b) {
    return a + b;
}

function sum2(a, b, c, d) {
    return a + b;
}

console.log('The sum is ' + (
    sum2(2, 2, 4, 5, 78)
));

// objects are first class citizens  in OOPs
/*
 1. Objects can be assigned to variables 
    Car car = new Car();

 2. Objects can be passed to methods
     calculateSpeedOfCar(car);
 
 3. Function can return an Object 
 
*/
