var car = (function () {

    var speed = 0;
    var color = 'green';
    var chassiNumber = 878766587;

    return {
        accelrate: function () {
            speed++;
        },

        deccelerate: function () {
            speed--;
        },

        halt: function () {
            speed = 0;
        },

        setColor: function (c) {
            color = c;
        },

        getColor: function () {
            return color;
        },

        getSpeed: function () {
            return speed;
        },

        getChassieNumber: function () {
            return chassiNumber;
        }

    }
})()


car.accelrate();
car.accelrate();
car.accelrate();
car.accelrate();
car.accelrate();

car.setColor("green");
console.log(car.getColor())


console.log(car.getSpeed())


car.deccelerate();
car.deccelerate();
console.log("After slowing down :: " + car.getSpeed())
// cannot access variables
console.log(car.speed);
