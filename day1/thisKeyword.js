// this keyword in the context of a function
'use strict';
function greet(message, suffix) {
    console.log("The value of this is ");
   // console.log(message + " " + this.name + " " + suffix);
    this.message = message;
}

// greet("", "");


// In case of an object
var user = {
    name: "Pramod",
    age: 25,

    getName: function () {
        return this.name;
    }
}

// console.log("User name is " + user.name);

// three ways you can reset the value of the this keyword

// 1. using call
// greet.call(user, "Welcome", " To the JS class");
// 2. using the apply
// greet.apply(user, ["Welcome", "to the JS class"])

// 3. bind
// var output = greet.bind(user, "Welcome", "to the JS class");

// console.log(output());

greet("", "");
